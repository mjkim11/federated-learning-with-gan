from collections import OrderedDict
import warnings

import flwr as fl
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms as transforms
import numpy as np
from torch.nn import GroupNorm
from torch.utils.data import DataLoader
from torchvision.datasets import CIFAR10
from torchvision.models import resnet18
from imutils import build_montages
import tqdm

from model import *


warnings.filterwarnings("ignore", category=UserWarning)
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)


def train(netG, netD, trainloader, epochs):
    accuracy = 0
    netG, netD = netG.to(DEVICE), netD.to(DEVICE)
    netD.apply(weights_init)
    netG.apply(weights_init)
    criterion = torch.nn.BCELoss()
    optimizerD = torch.optim.Adam(netD.parameters(), lr = 0.001, betas=(0.5, 0.999))
    optimizerG = torch.optim.Adam(netG.parameters(), lr=0.001, betas=(0.5, 0.999))
    real_label = 1
    fake_label = 0
    netG.train()
    netD.train()
    for idx in range(1,epochs+1):
        lossG_item = 0
        lossD_item = 0
        print(len(trainloader))
        for i, (images, labels) in enumerate(trainloader):
            images, labels = images.to(DEVICE), labels.to(DEVICE)
            netD.zero_grad()

            output = netD(images)
            b_size = images.size(0)
            label = torch.full((b_size,), real_label, dtype=torch.float, device=DEVICE)

            lossD_real = criterion(output, label)
            lossD_real.backward()
            noise = torch.randn(b_size,100,1,1, device=DEVICE)    
            fake = netG(noise)
            output = netD(fake.detach())
            #label = torch.zeros_like(output)
            label.fill_(fake_label)
            lossD_fake = criterion(output, label)
            lossD_fake.backward()
 
            lossD = lossD_fake + lossD_real
            optimizerD.step()

            netG.zero_grad()
            label.fill_(real_label)
            output = netD(fake)
            lossG = criterion(output,label)
            lossG.backward()
            optimizerG.step()
            #print(lossG.item())
            lossG_item += lossG.item()

            print(f'epoch:{idx}/{epochs}, iter:{i}/{len(trainloader)}, lossG:{lossG.item()}, lossD:{lossD.item()}')

        #print(f"Epochs : {idx},  Loss : {lossG_item}")
        if (idx == epochs) :
            return lossG_item, accuracy

def test(netG, netD, testloader):
    """Validate the network on the entire test set."""
    criterion = torch.nn.BCELoss()
    optimizerD = torch.optim.Adam(netD.parameters(), lr = 0.001, betas=(0.5, 0.999))
    optimizerG = torch.optim.Adam(netG.parameters(), lr=0.001, betas=(0.5, 0.999))
    real_label = 1
    fake_label = 0
    netD.eval()
    netG.eval()
    with torch.no_grad():
        for images, labels in testloader:
            images, labels = images.to(DEVICE), labels.to(DEVICE)
            outputs = netG(images)
            loss += criterion(outputs, labels).item()
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
    loss /= len(testloader.dataset)
    accuracy = correct / total
    return loss, accuracy


def load_data():
    """Load CIFAR-10 (training and test set)."""
    transform = transforms.Compose(
        [transforms.Resize(64),
         transforms.ToTensor(),
         transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    )
    trainset = CIFAR10("./dataset", train=True, download=True, transform=transform)
    testset = CIFAR10("./dataset", train=False, download=True, transform=transform)
    trainloader = DataLoader(trainset, batch_size=32, shuffle=True)
    testloader = DataLoader(testset, batch_size=32)
    num_examples = {"trainset": len(trainset), "testset": len(testset)}
    return trainloader, testloader, num_examples


def main():
    """Create model, load data, define Flower client, start Flower client."""
    netG = Generator()
    netD = Discriminator()
    # Load data (CIFAR-10)
    trainloader, testloader, num_examples = load_data()

    # Flower client
    class CifarClient(fl.client.NumPyClient):
        ## 현재 모델의 파라미터를 서버로 반환
        def get_parameters(self):
            return [val.cpu().numpy() for _, val in netG.state_dict().items()]

        ##
        def set_parameters(self, parameters):
            params_dict = zip(netG.state_dict().keys(), parameters)
            state_dict = OrderedDict({k: torch.tensor(v) for k, v in params_dict})
            netG.load_state_dict(state_dict, strict=True)
        ## 로컬 데이터셋으로 학습 시킴/ 리턴으로 
        def fit(self, parameters, config):
            self.set_parameters(parameters)
            _ = train(netG, netD, trainloader, epochs=1)
            return self.get_parameters(), num_examples["trainset"], {}

        def evaluate(self, parameters, config):
            self.set_parameters(parameters)
            loss, acc = train(netG, netD, trainloader, epochs=1)
            return float(loss),num_examples["trainset"], {"accuracy": float(acc)}

    # Start client
    fl.client.start_numpy_client("[::]:8080", client=CifarClient())


if __name__ == "__main__":
    main()